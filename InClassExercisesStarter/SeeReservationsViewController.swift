//
//  SeeReservationsViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore

class SeeReservationsViewController: UIViewController {

    
    //MARK: Outlets
    @IBOutlet weak var textField: UITextView!
    
    
    // MARK: Firebase variables
    var db:Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("You are on the see reservations screen")
        
        db = Firestore.firestore()
        
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        
        db.collection("reservation").getDocuments {
            
            (querySnapshot, err) in
            
            self.textField.text = "Look at the terminal for output!"
            
            if (err != nil) {
                print("Error!")
                print(err?.localizedDescription)
            }
            else {
                
                for x in (querySnapshot?.documents)! {
                    // Example 1: output row id
                    print(x.documentID)
                    
                    // Example 2: output all row contents
                    print(x.data())
                    print("----")
                    
                    let d = x.data();
                    print(d["name"])
                    
                    self.textField.text = "Name : \(d["name"] as! String),Day : \(d["day"] as! String), reatuarent : \(d["restaurent"] as! String), Seats : \(d["seats"] as! String)"
                    
                }
                
                
            }
            
            
        }
        
        
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
